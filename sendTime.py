#!/usr/bin/env python3

from select import poll
import serial
from datetime import datetime
from dateutil.relativedelta import relativedelta, MO
from time import sleep

def format_line(dt, front_matter):
    screen_width = 21
    delta = relativedelta(dt, datetime.now())
    string = "{}{:02d}d:{:02d}h:{:02d}m:{:02d}s".format(
        front_matter, delta.days, delta.hours, delta.minutes, delta.seconds)
    string += " "*(screen_width-len(string))
    return string

def build_times():
    dt_polls_close = datetime(2020, 11, 3, 19, 0, 0)
    dt_inaguration = datetime(2021, 1, 20, 9, 0, 0)     # 9am PST - noon EST
    dt_thanksgiving = datetime(2020, 11, 26, 18, 0, 0)  # 6pm nov 26th - dinner
    dt_apple_cup = datetime(2020, 11, 27, 16, 30, 0)    # 4:30pm nov 27th - kickoff
    dt_christmas = datetime(2020, 12, 25, 8, 0, 0)      # 8am dec 25th - chrismas!

    times = []
    times.append((dt_polls_close, "Elec: "))
    times.append((dt_thanksgiving, "Gobl: "))
    times.append((dt_apple_cup, "FtBl: "))
    times.append((dt_christmas, ("Xmas: ")))
    times.append((dt_inaguration, "Inag: "))
    return times

if __name__ == "__main__":
    print("hello world")

    ser = serial.Serial('/dev/ttyUSB1', 115200)
    print(ser.name)

    times = build_times()

    while True:
        upload_string = ""
        for time in times:
            upload_string += format_line(time[0], time[1])
        print(upload_string)
        ser.write(upload_string.encode('utf-8'))
        sleep(1)

    ser.close()

    print("done")
